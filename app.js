const express = require('express');
const cors = require('cors');

const skills = require('./static/skills');
const attributes = require('./static/attributes');

const app = express();
const port = 3000;

const mongoUri = 'mongodb://mongodb:27017/'

const { MongoClient } = require('mongodb');

async function run() {
    const client = new MongoClient(mongoUri);
    try {
      // Connect the client to the server
      await client.connect();
      // Establish and verify connection
      await client.db("admin").command({ ping: 1 });
      console.log("Connected successfully to server");

      const cthulhuDb = client.db("cthulhu");
      const collections = await cthulhuDb.listCollections({}).toArray();
      if (!collections.find(collection => collection.name === 'characters')) {
        await cthulhuDb.createCollection("characters");
        await cthulhuDb.collection("characters").insertOne({ name: "Fritz", occupation: "Donkey" });
      }
    } finally {
      // Ensures that the client will close when you finishdb/error
      await client.close();
    }
  }
run().catch(console.dir);

app.use(cors());
app.use(express.json());

app.get('/skills', (_, res) => {
    res.json(skills);
});

app.get('/characters', async(_, res) => {
    const client = new MongoClient(mongoUri);
    try {
        await client.connect();
        const cthulhuDb = client.db("cthulhu");
        const result = await cthulhuDb.collection("characters").find({}).toArray();
        res.json(result);
    } finally {
        await client.close();
    }
});

app.post('/characters', async(req, res) => {
    const character = req.body;
    if (!character.name || !character.occupation) {
        res.status(400).send("Character needs to have name and occupation");
		return;
    }

    const client = new MongoClient(mongoUri);

    try {
        await client.connect();
        const cthulhuDb = client.db("cthulhu");
        await cthulhuDb.collection("characters").insertOne(character);
        res.json(character);
    } finally {
        await client.close();
    }
});

app.get('/attributes', (_, res) => {
    res.json(attributes);
});

app.route('/translations/:lang').get((req, res) => {
    try {
        res.json(require('./static/translations/' + req.params.lang +  '.json'));
    }
    catch(error) {
        res.status(404);
        res.json({'error': 'Language ' + req.params.lang +  ' not found'});
    }
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})
