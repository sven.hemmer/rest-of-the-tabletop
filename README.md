# A tiny REST-API for table tops

## Run locally
Clone this repository, run 
***``` npm install ```***
and 
***``` npm run start ```***.  
The server will listen on port 3000.

## Endpoints
### ``` GET /attributes ```
Gets objects with attributes for all supported games. Eg.:
. Eg.:
```
{
  "cthulhu": {
	  int: {
		rule: '2d6 + 6'
      },
      ...
  },
  ...
}
```
### ``` GET /skills ```
Gets a list of skills for all supported games/categories. Eg.:
```
{
  "cthulhu": {
    "1920s": [
        {
            "key": "anthropology",
            "base": "1"
        },
        ...
    ],
  },
  ...
}
```
### ``` GET /translations/:lang ```
Gets a translation for all given games. Supported languages are ***en*** and ***de***.