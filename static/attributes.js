module.exports = {
	"cthulhu": {
		str: {
			rule: '3d6'
		},
		dex: {
			rule: '3d6'
		},
		int: {
			rule: '2d6 + 6'
		},
		con: {
			rule: '3d6'
		},
		app: {
			rule: '3d6'
		},
		pow: {
			rule: '3d6'
		},
		siz: {
			rule: '2d6 + 6'
		},    
		edu: {
			rule: '2d6 + 6'
		}
	}
}