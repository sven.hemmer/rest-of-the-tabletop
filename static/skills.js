
const cthulhu1920skills = {
    '0': [
        'credit', 'cthulu'   
    ],
    '1': [
        'anthropology', 'archeology', 'flanguage', 'locksmith', 'medicine', 'ophmachine', 'pilot', 'psyana', 'science'
    ],
    '5': [
        'accounting', 'appraise', 'art', 'disguise', 'history', 'law', 'occult', 'ride'
    ],
    '10': [
        'electr', 'mechr', 'natworld', 'navigate', 'persuade', 'psychology', 'sleightofhand', 'track'
    ],
    '15': [
        'charm', 'intimidate'
    ],
    '20': [
        'climb', 'drive', 'jump', 'library', 'listen', 'stealth', 'swim', 'throw'
    ],
    '25': [
        'spothidden'
    ],
    '30': [
        'faid'
    ],
    'DEX/2': 
    [
        'dodge'
    ],
    'EDU': [
        'olanguage'
    ]
}

module.exports = {
    cthulu: {
        '1920s': createSkillsObject(cthulhu1920skills)
    }
}

function createSkillsObject(skills) {
    return Object.entries(skills)
        .flatMap(([base, keys]) => 
            keys.map(key => createSkill(key, base)));
} 

function createSkill(key, base) {
    return { key, base };
}